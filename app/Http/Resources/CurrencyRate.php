<?php /** @noinspection PhpMissingParamTypeInspection */

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyRate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     * @noinspection PhpUndefinedFieldInspection
     */
    public function toArray($request)
    {
        return [
            'recordId'  =>  $this->id,
            'name'      =>  $this->name,
            'code'      =>  $this->code,
            'value'     =>  $this->value,
            'date'      =>  $this->date
        ];
    }
}

<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    /**
     *
     * @api {post} /api/auth
     * @apiName Authorization
     * @apiGroup Auth
     *
     * @apiParam {String} email User email.
     * @apiParam {String} password User password.
     *
     * @apiSuccess {String} token Result of creating.
     *
     * @apiSuccessExample Success-Response:
     *        HTTP/1.1 200 OK
     *        {
     *            '3|6kiqhWAMYpxm2RLUBpeiHsZWahXn83c5WhrUKI7y'
     *        }
     *
     */
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @return array|Response
     */
    public function __invoke(Request $request)
    {
        $data = $request->all();
        $validation = Validator::make($data, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        if( $validation->fails()) return $validation->errors()->toArray();


        $user = User::where('email', $data['email'])->first();

        if (! $user || ! Hash::check($data['password'], $user->password)) {
            $msg = new MessageBag(['email' => ['The provided credentials are incorrect.']]);
            return $msg->toArray();
        }

        return $user->createToken('api')->plainTextToken;
    }
}

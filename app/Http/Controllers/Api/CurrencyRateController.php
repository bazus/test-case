<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CurrencyRate;
use App\Http\Resources\CurrencyRate as CurrencyRateResource;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Validator;

class CurrencyRateController extends Controller
{

    /**
     * @api {get} /api/rates Index
     * @apiName RatesIndex
     * @apiGroup Rates
     *
     * @apiHeader (Authorization) {String} Token Bearer token.
     *
     * @apiParam {String} [name] Full name of currency.
     * @apiParam {String} [code] 3-symbols code of currency.
     * @apiParam {Date} [fromDate] Date start (DD.MM.YYYY).
     * @apiParam {Date} [toDate] Date finish (DD.MM.YYYY).
     * @apiParam {Integer} [onPage] Records count per 1 request.
     *
     * @apiSuccess {Integer} recordId Unique id.
     * @apiSuccess {String} name Full name of currency.
     * @apiSuccess {String} code 3-symbols code of currency.
     * @apiSuccess {Float} value Currency = value * 1 UAH.
     * @apiSuccess {Date} date Date of record.
     *
     * @apiSuccessExample Success-Response:
     *		HTTP/1.1 200 OK
     *		{
     *			{
     *				"recordId": 1,
     *				"name": "Долар США",
     *				"code": "USD",
     *				"value": 28.3989,
     *				"date": "28.08.2020"
     *			},
     *			{
     *				"recordId": 2,
     *				"name": "Євро",
     *				"code": "EUR",
     *				"value": 33.3133,
     *				"date": "28.08.2020"
     *			},
     *		}
     */
    /**
     * @param Request $req
     * @return array|AnonymousResourceCollection
     */
    public function index(Request $req){
        $data = $req->all();

        $validation = Validator::make($data,
            [
                'name'      =>  ['max:255', 'string'],
                'code'      =>  ['string'],
                'fromDate'  =>  ['date'],
                'toDate'    =>  ['date'],
                'onPage'    =>  ['min:1', 'numeric']
            ]);
        if( $validation->fails()) return $validation->errors()->toArray();

        $findRules = [];

        if(isset($data['name']))
            array_push($findRules, ['name', '=', $data['name']]);

        if(isset($data['code']))
            array_push($findRules, ['code', '=', $data['code']]);

        if(isset($data['fromDate']))
            array_push($findRules,
                ['date', '>', Carbon::parse($data['fromDate'])->toDate()]);

        if(isset($data['toDate']))
            array_push($findRules,
                ['date', '<', Carbon::parse($data['toDate'])->toDate()]);

        return CurrencyRateResource::collection(
            CurrencyRate::where(
                $findRules
            )->paginate(isset($data['onPage'])?$data['onPage']:15)
        );
    }


    /**
     * @api {get} /api/rates/:recordId Show
     * @apiName RatesShow
     * @apiGroup Rates
     *
     * @apiHeader (Authorization) {String} Token Bearer token.
     *
     * @apiParam {Integer} recordId Unique id.
     *
     * @apiSuccess {Integer} recordId Unique id.
     * @apiSuccess {String} name Full name of currency.
     * @apiSuccess {String} code 3-symbols code of currency.
     * @apiSuccess {Float} value Currency = value * 1 UAH.
     * @apiSuccess {Date} date Date of record.
     *
     * @apiSuccessExample Success-Response:
     *		HTTP/1.1 200 OK
     *		{
     *			"recordId": 1,
     *			"name": "Долар США",
     *			"code": "USD",
     *			"value": 28.3989,
     *			"date": "28.08.2020"
     *		}
     *
     * @apiError CurrencyRateNotFound The record with this id not found.
     * @apiErrorExample Error-Response:
     *      HTTP/1.1 404 Not Found
     *      {
     *      }
     */
    /**
     * @param $id
     * @return CurrencyRateResource
     */
    public function show($id){
        return
            CurrencyRateResource::make(
                CurrencyRate::findOrFail($id)
            );
    }


    /**
     * @api {post} /api/rates/ Store
     * @apiName RatesStore
     * @apiGroup Rates
     *
     * @apiHeader (Authorization) {String} Token Bearer token.
     *
     * @apiParam {String} name Full name of currency.
     * @apiParam {String} code 3-symbols code of currency.
     * @apiParam {Float} value Currency = value * 1 UAH.
     * @apiParam {Date} date Date of record (DD.MM.YYYY).
     *
     * @apiSuccess {Bool} result Result of creating.
     *
     * @apiSuccessExample Success-Response:
     *		HTTP/1.1 200 OK
     *		{
     *			'result' => true
     *		}
     */
    /**
     * @param Request $req
     * @return bool[]
     */
    public function store(Request $req){
        $data = $req->all();
        $validation = Validator::make($data,
            [
                'name'  =>  ['required', 'max:255', 'string'],
                'code'  =>  ['required', 'string'],
                'value' =>  ['required', 'numeric'],
                'date'  =>  ['required', 'date']
            ]);

        if( $validation->fails()) return $validation->errors()->toArray();

        $data['date'] = Carbon::create($data['date'])->toDate();

        $currencyRate   =   new CurrencyRate($data);
        $saveResult     =   (bool)$currencyRate->save();

        return [
            'result'    =>  $saveResult
        ];
    }


    /**
     *
     * @api {patch} /api/rates/:recordId Update
     * @apiName RatesUpdate
     * @apiGroup Rates
     *
     * @apiHeader (Authorization) {String} Token Bearer token.
     *
     * @apiParam {Integer} recordId Unique id.
     * @apiParam {String} name Full name of currency.
     * @apiParam {String} code 3-symbols code of currency.
     * @apiParam {Float} value Currency = value * 1 UAH.
     * @apiParam {Date} date Date of record (DD.MM.YYYY).
     *
     * @apiSuccess {Bool} result Result of creating.
     *
     * @apiSuccessExample Success-Response:
     *        HTTP/1.1 200 OK
     *        {
     *            'result' => true
     *        }
     *
     *
     * @apiError CurrencyRateNotFound The record with this id not found.
     * @apiErrorExample Error-Response:
     *      HTTP/1.1 404 Not Found
     *      {
     *      }
     */
    /**
     * @param Request $req
     * @param $id
     * @return bool[]
     */
    public function update(Request $req, $id){
        $data = $req->all();
        $validation = Validator::make($data,
            [
                'name'  =>  ['max:255', 'string'],
                'code'  =>  ['string'],
                'value' =>  ['numeric'],
                'date'  =>  ['date']
            ]);

        if( $validation->fails()) return $validation->errors()->toArray();
        if(isset($data['date']))
            $data['date'] = Carbon::create($data['date'])->toDate();

        return [
            'result'    => (bool)CurrencyRate::findOrFail($id)
                ->update($data)
        ];
    }


    /**
     *
     * @api {delete} /api/rates/:recordId Delete
     * @apiName RatesDelete
     * @apiGroup Rates
     *
     * @apiHeader (Authorization) {String} Token Bearer token.
     *
     * @apiParam {Integer} recordId Unique id.
     *
     * @apiSuccess {Bool} result Result of creating.
     *
     * @apiSuccessExample Success-Response:
     *        HTTP/1.1 200 OK
     *        {
     *            'result' => true
     *        }
     *
     * @apiError CurrencyRateNotFound The record with this id not found.
     * @apiErrorExample Error-Response:
     *      HTTP/1.1 404 Not Found
     *      {
     *      }
     */
    /**
     * @param $id
     * @return bool[]
     */
    public function destroy($id){
        return [
            'result' => (bool)CurrencyRate::findOrFail($id)
                ->delete()
        ];
    }
}

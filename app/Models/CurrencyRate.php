<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Models;

use Carbon\{Carbon, CarbonPeriod};
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class CurrencyRate extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function getValueAttribute($value){
        return $value / 10000;
    }

    /**
     * @param $value
     */
    public function setValueAttribute($value){
        $this->attributes['value'] = (int)($value * 10000);
    }

    public static function getRates(Carbon $fromDate = null, Carbon $toDate = null){
        if( null === $fromDate ) $fromDate = Carbon::today();
        if( null === $toDate ) $toDate = Carbon::today();

        $period = new CarbonPeriod($fromDate, $toDate);

        foreach ($period as $date){

            $response = Http::get('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange', [
                'date' => $date->format('Ymd'),
                'json'
            ])->json();

            foreach ($response as $record){
                self::create(
                    [
                        'name' => $record['txt'],
                        'code' => $record['cc'],
                        'date' => $date->toDate(),
                        'value' => $record['rate']
                    ]
                );
            }
        }


    }
}

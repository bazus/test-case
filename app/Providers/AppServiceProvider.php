<?php

namespace App\Providers;

use App\Models\CurrencyRate;
use App\Observers\CurrencyRateObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        CurrencyRate::observe(CurrencyRateObserver::class);
    }
}

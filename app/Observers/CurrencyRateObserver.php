<?php /** @noinspection PhpUndefinedMethodInspection */

namespace App\Observers;

use App\Models\CurrencyRate;

class CurrencyRateObserver
{
    /**
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function creating(CurrencyRate $currencyRate){

        CurrencyRate::where('code', $currencyRate->code)
            ->where('date', $currencyRate->date)
            ->delete();
    }

    /**
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function updating(CurrencyRate $currencyRate){
        CurrencyRate::where('code', $currencyRate->code)
            ->where('date', $currencyRate->date)
            ->delete();
    }
    /**
     * Handle the CurrencyRate "created" event.
     *
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function created(CurrencyRate $currencyRate)
    {
        //
    }

    /**
     * Handle the CurrencyRate "updated" event.
     *
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function updated(CurrencyRate $currencyRate)
    {
        //
    }

    /**
     * Handle the CurrencyRate "deleted" event.
     *
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function deleted(CurrencyRate $currencyRate)
    {
        //
    }

    /**
     * Handle the CurrencyRate "restored" event.
     *
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function restored(CurrencyRate $currencyRate)
    {
        //
    }

    /**
     * Handle the CurrencyRate "force deleted" event.
     *
     * @param CurrencyRate $currencyRate
     * @return void
     */
    public function forceDeleted(CurrencyRate $currencyRate)
    {
        //
    }
}

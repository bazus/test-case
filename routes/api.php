<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\{Api};

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('auth', Api\AuthController::class);

Route::middleware('auth:sanctum')->group(function (){

    Route::apiResource('rates', Api\CurrencyRateController::class);

});



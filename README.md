
### Instalation
| Requirements |
| ------ |
| PHP >= 7.3 |
| Ctype PHP Extension | 
| Fileinfo PHP Extension |
| BCMath PHP Extension |
| XML PHP Extension |
| Tokenizer PHP Extension | 
| PDO PHP Extension | 
| OpenSSL PHP Extension |
| JSON PHP Extension |
| Mbstring PHP Extension| 

```sh
$ composer install
$ php artisan key:generate
$ cp .env.example .env
$ vi .env // configure db
$ php artisan migrate
$ php artisan rates:get // get rates for last month
$ php artisan schedule:run >> /dev/null 2>&1
```
